============================================================
=                        SPL4T                             =
============================================================
*************Developed by Dragon Sloth Studios**************
============================================================
=                     Samuel Buckle                        =
=			Darian Tse                         =
=		     Curtis Rio Sewell                     =
=		        Gabe Ramsay                        =
=		      Alvin Varghese                       =
============================================================
////////////////////////////////////////////////////////////
/A four-player unilateral arcade game, in which each player/ 
/  takes control of enviromental power ups to splat your   /
/            oppoments before they splat you!              /
////////////////////////////////////////////////////////////
____________________________________________________________
________________________CONTROLS____________________________
=                                                          =
=                       Player1                            =
=                                                          =
____________________________________________________________
=            aim countercolockwise = key "a"               =
=                aim clockwise = key "s"                   =
=                   shoot = key "d"                        =
=               powerup slot 1 = key "r"                   =
=               powerup slot 2 = key "t"                   =
=               aim powerup = mouse                        =
=         use selected powerup = mouse button 1            =
____________________________________________________________
=                                                          =
=                       Player2                            =
=                                                          =
____________________________________________________________
=        aim countercolockwise = joystick button 4         =
=            aim clockwise = joystick button 5             =
=                shoot = joystick button 1                 =
=           powerup slot 1 = joystick button 0             =
=           powerup slot 2 = joystick button 3             =
=             aim powerup = analog stick                   =
=         use selected powerup = joystick button 1         =
____________________________________________________________
///////////////GOOD LUCK AND HAVE FUN!//////////////////////