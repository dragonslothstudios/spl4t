﻿using UnityEngine;
using System.Collections;

public class MoveRandom : MonoBehaviour {

    Vector3 randomDir;

    private Rigidbody rb;

    

	// Use this for initialization
	void Start () {

        rb = GetComponent<Rigidbody>();

        Vector3 randomDir = Random.onUnitSphere;

        rb.velocity = randomDir;
        
    }
	
	// Update is called once per frame
	void Update () {

        rb.velocity = 3 * (rb.velocity.normalized);

    }
}
