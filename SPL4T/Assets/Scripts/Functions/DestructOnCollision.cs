﻿using UnityEngine;
using System.Collections.Generic;

public class DestructOnCollision : MonoBehaviour
{
    //public List<GameObject> deactivatedTiles = new List<GameObject>();

    void OnCollisionEnter(Collision col)
    {
        //Destroy(col.gameObject);

        //deactivatedTiles.Add(col.gameObject);
       // print(col.gameObject);

        if (col.gameObject.name == "Floor_Tile")
        {
            col.gameObject.SetActive(false);
        }
    }
}
