﻿using UnityEngine;
using System.Collections;

public class Projection : MonoBehaviour {

    public Projector proj;
    public Material mat1;
    public Material mat2;
    Camera viewCam;
    public int playerNum;
    bool inBoundary;
    float size = 10.0f;
   
    // Use this for initialization
    void Start () { 
        viewCam = Camera.main;
        proj.GetComponent<Projector>();

        if(playerNum == 1)
        {
            proj.material = mat1;
        }
        else if(playerNum == 2)
        {
            proj.material = mat2;
        }
    }    
    
    public void setPlayer(int _playerNum)
    {
        playerNum = _playerNum;
    }    

    // Update is called once per frame
    void Update()
    {
        if (playerNum == 1)
        {
           
            Ray ray = viewCam.ScreenPointToRay(Input.mousePosition); //Main camera to mouse position

            RaycastHit hitInfo = new RaycastHit();

            bool hit = Physics.Raycast(ray, out hitInfo); //Send data to hitInfo
            if (hit)
            {
                Vector3 temp = new Vector3(hitInfo.point.x, hitInfo.point.y, hitInfo.point.z);
                transform.position = temp;
                //print("On Something");
            }
        }
        else if (playerNum == 2) 
        {   
            //this.transform.parent = null;

            if (Input.GetAxis("Horizontal") !=0 || Input.GetAxis("Vertical") != 0)
            {
                Vector3 oldpos = transform.position;
                Vector3 temp = new Vector3(transform.position.x + Input.GetAxis("Horizontal") / 4, transform.position.y, transform.position.z + Input.GetAxis("Vertical") / 4);

                if (temp.x < size && temp.z < size && temp.x > -size && temp.z > -size)
                {
                    inBoundary = true;
                }
                else
                {
                    inBoundary = false;
                }

                if (temp != oldpos && inBoundary)
                    transform.position = temp;
                else
                    transform.position = oldpos;
            }
        }

    }
}
