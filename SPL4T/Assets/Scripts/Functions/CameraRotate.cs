﻿using UnityEngine;
using System.Collections;

public class CameraRotate : MonoBehaviour {

    Camera cam;
    float degrees = 1.0f;


	// Use this for initialization
	void Start () {
	    
	}

    void FixedUpdate()
    {
        if(Input.GetKey("q"))
        {
            transform.RotateAround(Vector3.zero, Vector3.up, degrees);
        }
        if(Input.GetKey("e"))
        {
            transform.RotateAround(Vector3.zero, Vector3.up, -degrees);
        }
    }
	
	// Update is called once per frame
	void Update () {
   
    }
}
