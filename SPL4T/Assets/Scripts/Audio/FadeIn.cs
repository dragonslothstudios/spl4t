﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;


public class FadeIn : MonoBehaviour {

    AudioSource source0;

    // Use this for initialization
    void Start () {

        source0 = GetComponent<AudioSource>();

        source0.volume = 0.0f;

    }
	
	// Update is called once per frame
	void Update () {

        if(source0.volume < 0.5f)
        {
            source0.volume = Mathf.Lerp(source0.volume, 0.5f, Time.deltaTime);
        }
	
	}
}
