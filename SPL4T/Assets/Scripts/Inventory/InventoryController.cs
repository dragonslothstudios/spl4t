﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InventoryController : MonoBehaviour {

    //public Sprite[] sprites;

    public Transform selectedItem, selectedSlot, originalSlot;


    public GameObject slotPrefab;//,spritePrefab; //actual prefabs 
    public float slotSize; //size of slot (Dont change it)

    public GameObject slot1, slot2;
    //GameObject sprite;
    //public bool trigger;
    //private int tempSlotNumber, tempID;

    private Vector2 inventorySize = new Vector2(2, 1);
    private Vector2 windowSize = new Vector2 (75,50); //yea, its hardcoded

	// Create slots in the inventory area
	void Start () {
        // for(int x = 1; x <= inventorySize.x; x++)
        // {
        //     for(int y = 1; y <= inventorySize.y; y++)
        //     {
        //         GameObject slot = Instantiate(slotPrefab) as GameObject;
        //         slot.transform.parent = this.transform;
        //         slot.name = "slot_" + x + "_" + y;
        //         // slot.GetComponent<RectTransform>().anchoredPosition = new Vector3(windowSize.x / (inventorySize.x + 1) * x, windowSize.y / (inventorySize.y + 1) * -y, 0);
        //         slot.GetComponent<RectTransform>().anchoredPosition = new Vector3(windowSize.x/(inventorySize.x+1) + slotSize * (x-1), windowSize.y / (inventorySize.y+1) * -y, 0);
        //     }
        // }
        slot1 = Instantiate(slotPrefab) as GameObject;
        slot1.transform.parent = this.transform;
        slot1.name = "slot1";
        slot1.GetComponent<RectTransform>().anchoredPosition = new Vector3(windowSize.x / (inventorySize.x + 1), windowSize.y / (inventorySize.y + 1) * -1, 0);
        
        
        slot2 = Instantiate(slotPrefab) as GameObject;
        slot2.transform.parent = this.transform;
        slot2.name = "slot2";
        slot2.GetComponent<RectTransform>().anchoredPosition = new Vector3(windowSize.x / (inventorySize.x + 1) + slotSize , windowSize.y / (inventorySize.y + 1) * -1, 0);
        //CreateSprite(2, 1);
        //trigger = false;
    }

    //public void TriggerCreation(int slot_Number, int sprite_ID)
    //{ 
    //    trigger = true;
    //    tempSlotNumber = slot_Number;
    //    tempID = sprite_ID;
    //}

    //public void CreateSprite(int slotNumber, int spriteID)
    //{
    //
    //    //GameObject sprite = Instantiate(spritePrefab) as GameObject;
    //    sprite = Instantiate(spritePrefab) as GameObject;
    //    if (slotNumber == 1)
    //    {
    //       sprite.transform.parent = slot1.transform;
    //    }
    //    else if (slotNumber == 2)
    //    {
    //        sprite.transform.parent = slot2.transform;
    //    }
    //   
    //    //Sprite component
    //    sprite.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
    //    sprite.GetComponent<Image>().sprite = sprites[spriteID];
    //}

	// Update is called once per frame
	void Update () {

        //if(trigger)
        //{
        //    print("Sprite Created");
        //    CreateSprite(tempSlotNumber, tempID);
        //    trigger = false;
        //}

        //if mouse click
        if (Input.GetMouseButtonDown(0) && selectedItem != null)
        {
            originalSlot = selectedItem.parent;
            selectedItem.GetComponent<Collider>().enabled = false;
        }

        //if mouse held down, move sprite
        if (Input.GetMouseButton(0) && selectedItem != null)
        {
            selectedItem.position = Input.mousePosition;
        }
        //reset pos/reassign parent if let go of left click
        else if (Input.GetMouseButtonUp(0) && selectedItem != null)
        {
            if (selectedSlot == null)
            {
                selectedItem.parent = originalSlot;
            }
            else
            {
                selectedItem.parent = selectedSlot;
            }
            selectedItem.localPosition = Vector3.zero;
            selectedItem.GetComponent<Collider>().enabled = true;   
        }

	}

}
