﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour {

    public GameObject gameManager;          //GameManager prefab to instantiate.

    void Awake()
    {
       // Scene scene = SceneManager.GetActiveScene();
       // SceneManager.LoadScene("LevelGenerator");
        //Check if a GameManager has already been assigned to static variable GameManager.instance or if it's still null
        if (GameManager.instance == null)

            //Instantiate gameManager prefab
            Instantiate(gameManager);


    }

}
