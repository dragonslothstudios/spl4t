﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Runtime.InteropServices;
using System.Collections;

public class PlayerController : MonoBehaviour
{

    [DllImport("DLL")]
    public static extern float ReturnSpeed();

    public ParticleSystem ps;
    public Material particle1;
    public Material particle2;

    public int playerNumber; //Identify players

    public float speed;   //speed of ball
    public float turnSpeed; //How fast balls turn (aiming)

    public float bouncePadPower;
    public float tornadoPower;

    public Renderer renderMat;

    public Vector3 spawnLocation;

    public int lives = 5; 


    public bool isRespawning;
    public bool respawnBoth;
    public bool speedBoost;

    public bool gameOver;

    public GameObject AimingArrow;

    Vector3 AimSpawn;


    public Text deathCountP1;
    public Text deathCountP2;

    public bool shield;
    public float shieldTimer = 10.0f;
    private float shieldTime = 10.0f;

    public float boostTimer = 5.0f;
    private float boostTime = 5.0f;


    private Rigidbody rb; //reference to ball   

    private string turnAxisName; //Name of axis to turn, different players
    private float turnInputValue; //Value of turn input

    private bool shot; //If the player has shot the ball

    public string leftControl = "a";
    public string rightControl = "d";
    public string shootControl = "w";
    private float direction = 0.0f;

    //Camera viewCam; //Main Camera for player *player, not players....to be dealt with later

    //public GameObject projector;
    //public GameObject powerUp1; //falling rocks?

    //bool check; 

    //initializes values
    void Start()
    {

        rb = GetComponent<Rigidbody>();
        renderMat = GetComponent<Renderer>();

        rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionY;
        gameOver = false;
        
        if(playerNumber == 1)
        {
            AimingArrow = GameObject.Find("AimingArrow P1");
            AimSpawn = AimingArrow.transform.position;

            ps.GetComponent<ParticleSystemRenderer>().material = particle1;
        }
        else if(playerNumber == 2)
        {
            AimingArrow = GameObject.Find("AimingArrow P2");
            AimSpawn = AimingArrow.transform.position;

            ps.GetComponent<ParticleSystemRenderer>().material = particle2;
        }

        speed = ReturnSpeed();
        //speed = 5;
        turnSpeed = 90.0f;
        turnInputValue = 0.0f;
        shot = false;
        shield = false;
        lives = 5;
        speedBoost = false;
        isRespawning = true;
        respawnBoth = false;

        bouncePadPower = 20.0f;
        tornadoPower = 30.0f;


        turnAxisName = "Horizontal";// + playerNumber;

        shieldTime = shieldTimer;

        deathCountP1 = GameObject.Find("Lives Count P1").GetComponent<Text>();
        deathCountP2 = GameObject.Find("Lives Count P2").GetComponent<Text>();

        deathCountP1.text = "P1 Lives: " + lives;
        deathCountP2.text = "P2 Lives: " + lives;

        Material newMat = Resources.Load("DEV_Orange", typeof(Material)) as Material;
        
       // gameObject.renderer.material = newMat;

        //viewCam = Camera.main;

        //check = false;
        //projector.SetActive(false);
    }

    public void setControls(string left, string right, string shoot)
    {
        leftControl = left;
        rightControl = right;
        shootControl = shoot;

    }

    public void setPlayerNum(int num)
    {
        playerNumber = num;
    }


    void FixedUpdate()
    {
        Aim();
        Controls();
    }

    void Update()
    {
        //Misc control stuff
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        if(Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene("Menu");
        }


        // MouseControl();//Move into if statement later
        if (shot)
        {
            rb.velocity = speed * (rb.velocity.normalized); //Auto move after initial shot
        }
        else
        {
            turnInputValue = Input.GetAxisRaw(turnAxisName); //Getaxisraw for more snappy aiming
        }

        if(shield)
        {
            shieldTimer -= Time.deltaTime;
            speed = speed + 0.001f;
        }

        if(shieldTimer < 0)
        {
            shield = false;
            print("Shield off");
            shieldTimer = shieldTime;
        }


        if (rb.position.y < -5)
        {
            Respawn();
        }
        //deathCount.text = "Deaths: " + deaths.ToString();
        if (speedBoost)
        {
            boostTimer -= Time.deltaTime;
            speed = 10.0f;
        }
        if (boostTimer < 0)
        {
            speedBoost = false;
            speed = 5;
            print("Boost off");
            boostTimer = boostTime;
        }

    }

    void Respawn()
    {
        AimingArrow.SetActive(true);
        AimingArrow.transform.rotation = new Quaternion(0, -90, 90, 1);
        AimingArrow.transform.position = AimSpawn;
        shot = false;
        rb.velocity = new Vector3(0, 0, 0);
        rb.angularVelocity = new Vector3(0, 0, 0);
        rb.transform.rotation = Quaternion.identity;
        rb.transform.position = spawnLocation;
        rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionY;
        //print("X:" + spawnLocation.x + " Y:" + spawnLocation.y + " Z:" + spawnLocation.z);
        print("Respawning...");
        lives = lives - 1;
        FixedUpdate();

        if (playerNumber == 1)
        {
            deathCountP1.text = "P1 Lives: " + lives.ToString();
            print("P1 Died");

        }
        else if (playerNumber == 2)
        {
            deathCountP2.text = "P2 Lives: " + lives.ToString();
            print("P2 Died");
        }

    }


    void Aim()
    {
        if (!shot)
        {
            // Determine the number of degrees to be turned based on the input, speed and time between frames
            float turn = 0;
            turn = direction * turnSpeed * Time.deltaTime;
            //print(turnInputValue);

            // Make this into a rotation in the y axis
            Quaternion turnRotation = Quaternion.Euler(0f, turn, 0f);

            // Apply this rotation to the rigidbody's rotation
            rb.MoveRotation(rb.rotation * turnRotation);

            AimingArrow.transform.RotateAround(rb.position, new Vector3(0,1,0), turn);

           // AimingArrow.transform.LookAt(new Vector3(rb.transform.forward.x, 1, rb.transform.forward.z));
            //Debug.DrawLine(rb.position, rb.position + (rb.transform.forward * 20), Color.red);

        }
    }

    //Pointer for mouse
    //  void MouseControl()
    //  {
    //      if (Input.GetMouseButtonDown(0) && check)
    //      {
    //          Ray ray = viewCam.ScreenPointToRay(Input.mousePosition); //Main camera to mouse position
    //    
    //          RaycastHit hitInfo = new RaycastHit();
    //    
    //          bool hit = Physics.Raycast(ray, out hitInfo); //Send data to hitInfo
    //          if (hit)
    //          {
    //              //Debug.Log("Hit " + hitInfo.transform.gameObject.name);
    //              Debug.DrawLine(ray.origin, hitInfo.point, Color.blue);
    //
    //              Vector3 spawnPoint = hitInfo.point;
    //              spawnPoint.y += 10;
    //              Instantiate(powerUp1, spawnPoint, Quaternion.identity);
    //              
    //              //if (hitInfo.transform.gameObject.name == "FloorTile(Clone)")
    //              //{
    //              //    //Debug.Log("Tile Hit!");                   
    //              //    //Destroy(hitInfo.collider.gameObject);
    //              //    hitInfo.collider.gameObject.SetActive(false);
    //              //}
    //              //else
    //              //{
    //              //    //Debug.Log("Hit Something but not Tile");
    //              //}
    //          }
    //
    //          check = false;
    //          projector.SetActive(false);
    //      }
    //  }


    //Keyboard Input
    void Controls()
    {
        if (!gameOver)
        {
            if (Input.GetKeyUp(shootControl) && !shot)
            {
                rb.constraints = RigidbodyConstraints.None;
                shot = true;
                rb.velocity = rb.transform.forward * speed; //initial velocity
                isRespawning = false;
                respawnBoth = false;
                print("shot fired");
                AimingArrow.SetActive(false);
            }
            if (Input.GetKey(leftControl))
            {
                direction = -1.0f;
            }
            if (Input.GetKey(rightControl))
            {
                direction = 1.0f;
            }
            if (Input.GetKeyUp(leftControl) || Input.GetKeyUp(rightControl))
            {
                direction = 0.0f;
            }
        }
        //powerups
        //if (Input.GetKeyUp("r"))
        //{
        //    check = !check;
        //    if (check) { projector.SetActive(true); }
        //    else { projector.SetActive(false); }
        //}
        //if (Input.GetKeyUp("t"))
        //{
        //    if (speed == 10)
        //    {
        //        rb.constraints = RigidbodyConstraints.None;
        //        speed = 5;
        //    }
        //    else
        //    {
        //        rb.constraints = RigidbodyConstraints.FreezePositionY;
        //        speed = 10;
        //    }
        //}

    }



    void OnCollisionEnter(Collision collision)
    {
        if (shot)
        {
            if (collision.gameObject.name == "BouncePad(Clone)")
            {
                if (!shield)
                {
                    Vector3 jump;
                    jump.x = rb.velocity.x;
                    jump.y = rb.velocity.y + bouncePadPower;
                    jump.z = rb.velocity.z;
                    print("jump");

                    rb.velocity = jump;

                    rb.useGravity = true;
                }
                else
                {
                    Destroy(collision.gameObject);
                }

            }
            if (collision.gameObject.name == "Floor_Tile")
            {
                if (rb.velocity.y > 0)
                {
                    Vector3 land;
                    land.x = rb.velocity.x;
                    land.y = 0;
                    land.z = rb.velocity.z;

                    rb.velocity = land;
                }

            }
            if (collision.gameObject.name == "Debris(Clone)")
            {
                if (!shield)
                {
                    Respawn();
                }
            }
            if (collision.gameObject.name == "Player 1(Clone)" || collision.gameObject.name == "Player 2(Clone)")
            {
                if (collision.gameObject.GetComponent<PlayerController>().shield
                    || collision.gameObject.GetComponent<PlayerController>().speed > speed)
                {
                    if (!shield)
                    {
                        Respawn();
                        isRespawning = true;
                        print("Death to player");
                    }
                    return;
                }
                else
                {
                    if (collision.gameObject.GetComponent<PlayerController>().isRespawning)
                    {
                        return;
                    }
                    else if (collision.gameObject.GetComponent<PlayerController>().respawnBoth)
                    {
                        respawnBoth = true;
                        Respawn();
                        print("Same speed!");
                    }
                    else
                    {
                        respawnBoth = true;
                        Respawn();
                        print("Same speed?");
                    }

                }

            }
        }
        else
        {

        }
    }

    void OnTriggerEnter(Collider collider)
    {

        if (shot)
        {
            if (collider.gameObject.name == "Tornado(Clone)")
            {
                if (!shield)
                {
                    Vector3 random = Random.onUnitSphere * tornadoPower;

                    random.y = (random.y + 1) * 2;

                    rb.velocity = random;
                    print("Tornado");
                }
                else
                {
                    //Pass through tornado
                }
            }
        }
        else
        {
            //Pass through
        }
    }

}
   


        //public void setCheck(bool powerUpCheck)
        //{
        //    check = powerUpCheck;
        //}

//When collider/rigidbody begins touching
//void OnCollisionEnter(Collision collision)
//{
// foreach (ContactPoint contact in collision.contacts)//find collision point
// {           
//
// }
//
// if (collision.gameObject.name != "Tile")
// {
//     //Destroy(collision.gameObject);
//     //print("collided");
// }

//if (collision.gameObject.name == "Powerup")
//{
//    Destroy(collision.gameObject);
//    print("PickUp");
//}
//}

//When collider touches the trigger
//void OnTriggerEnter(Collider collider)
//{
//    if(collider.gameObject.name == "Powerup1")
//    {
//        print("picked up powerup");
//    }
//}
