﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PowerupControls : MonoBehaviour {

    private int slot1; //powerup slot1
    private int slot2; //powerup slot2
    Camera viewCam;

    

    public InventoryController ic;
    public Sprite[] sprites; //Stores array of sprites to pull from
    GameObject sprite; //Instantiating the sprite
    public GameObject spritePrefab; //The prefab of the sprite to reference from

    public GameObject projector; //aiming projector
    public Projection pj;
    public GameObject player;
    private GameObject proj;

    //powerup1 - drop object
    private int powerup1_Id = 1; //ID for debris
    private int powerup2_Id = 2; //ID for bouncepad
    private int powerup3_Id = 3; //ID for tornado
    private int powerup4_Id = 4; //ID for shield
    private int powerup5_Id = 5; //ID for speed boost
    public GameObject powerup1; //Debris
    public GameObject powerup2; //ForceJump block
    public GameObject powerup3; //Tornado cylinder
    bool powerup1Check;
    bool powerup2Check;
    bool powerup3Check;
    bool powerup4Check;
    bool powerup5Check;

    bool slot1Triggered;
    bool slot2Triggered;
    Vector3 spawnPoint;

    //Player controls
    PlayerController playerN;
    public string fire1 = "r";
    public string fire2 = "t";
    
    //Find last key pressed
    Event e;
    string lastKeyPressed = "";
    bool keydown = false;

    void OnGUI()
    {
        e = Event.current;
        if (e.type.Equals(EventType.KeyDown) && !keydown)
        {
            keydown = true;
            lastKeyPressed = e.keyCode.ToString();
        }

        if (e.type.Equals(EventType.KeyUp))
            keydown = false;

        GUILayout.Label("Last Key Pressed - " + lastKeyPressed);
    }



    // Use this for initialization
    void Start () {
        playerN = GetComponent<PlayerController>();
        if (playerN.playerNumber == 1)
        {
            fire1 = "r";
            fire2 = "t";
            ic = GameObject.Find("Inventory P1").GetComponent<InventoryController>();
        }
        else if (playerN.playerNumber == 2)
        {
            fire1 = "joystick button 0";
            fire2 = "joystick button 3";
            ic = GameObject.Find("Inventory P2").GetComponent<InventoryController>();
        }

       

        slot1 = 0;
        slot2 = 0;
        viewCam = Camera.main;
        projector.SetActive(false);
        Instantiate(projector, transform.position + new Vector3(0,5,0), Quaternion.Euler(new Vector3(90, 0, 0)));
        pj = projector.GetComponent<Projection>();
        pj.setPlayer(playerN.playerNumber);
        

        //powerup 1
        powerup1Check = false;
        powerup2Check = false;
        powerup3Check = false;
        powerup4Check = false;
        powerup5Check = false;


    }

    void FixedUpdate()
    {
        Controls();

        if (playerN.isRespawning)
        {
            deleteProjector();

            if (slot1 != 0)
            {
                slot1 = 0;
                DeleteSprite(1);
            }
            if (slot2 != 0)
            {
                slot2 = 0;
                DeleteSprite(2);
            }

            powerup1Check = false;
            powerup2Check = false;
            powerup3Check = false;
            powerup4Check = false;
            powerup5Check = false;
        }
    }

    void Update()
    {
        if(powerup1Check)
        {
            Powerup1();           
        }
        if(powerup2Check)
        {
            Powerup2();
        }
        if (powerup3Check)
        {
            Powerup3();
        }
        if(powerup4Check)
        {
            Powerup4();
        }
        if (powerup5Check)
        {
            Powerup5();
        }

    }

    public void instantiateProjector()
    {
        proj = (GameObject)Instantiate(projector, transform.position + new Vector3(0, 5, 0), Quaternion.Euler(new Vector3(90, 0, 0)));
        pj = proj.GetComponent<Projection>();
        pj.setPlayer(playerN.playerNumber);
        proj.SetActive(true);
    }
    public void deleteProjector()
    {
        if (proj)
        { 
                //proj.SetActive(false);
                Destroy(proj);
        }

        
    }

    void Controls()
    {
        if (!playerN.gameOver)
        {
            //powerups in slot 1
            if (Input.GetKeyUp(fire1))
            {
                slot1Triggered = true;
                //   projector.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                //check powerup 1
                if (slot1 == powerup1_Id)
                {
                    powerup1Check = !powerup1Check;
                    if (powerup1Check) { instantiateProjector(); }
                    else { deleteProjector(); }

                }
                if (slot1 == powerup2_Id)
                {
                    powerup2Check = !powerup2Check;
                    if (powerup2Check) { instantiateProjector(); }
                    else { deleteProjector(); }
                }
                if (slot1 == powerup3_Id)
                {
                    powerup3Check = !powerup3Check;
                    if (powerup3Check) { instantiateProjector(); }
                    else { deleteProjector(); }
                }
                if (slot1 == powerup4_Id)
                {
                    powerup4Check = !powerup4Check;
                    if (powerup4Check) { }
                    else { deleteProjector(); }
                }
                if (slot1 == powerup5_Id)
                {
                    powerup5Check = !powerup5Check;
                    if (powerup5Check) { }
                    else { deleteProjector(); }
                }


                //check empty
                else if (slot1 == 0)
                {
                    print("Nothing in slot 1");
                }
            }

            //------------------------------------------------------------------------------------//

            //powerups in slot 2
            if (Input.GetKeyUp(fire2))
            {
                slot2Triggered = true;
                // projector.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                //check powerup 1
                if (slot2 == powerup1_Id)
                {
                    powerup1Check = !powerup1Check;
                    if (powerup1Check) { instantiateProjector(); }
                    else { deleteProjector(); }

                    //Powerup1();
                }
                if (slot2 == powerup2_Id)
                {
                    powerup2Check = !powerup2Check;
                    if (powerup2Check) { instantiateProjector(); }
                    else { deleteProjector(); }
                }
                if (slot2 == powerup3_Id)
                {
                    powerup3Check = !powerup3Check;
                    if (powerup3Check) { instantiateProjector(); }
                    else { deleteProjector(); }
                }
                if (slot2 == powerup4_Id)
                {
                    powerup4Check = !powerup4Check;
                    if (powerup4Check) { }
                    else { deleteProjector(); }
                }
                if (slot2 == powerup5_Id)
                {
                    powerup5Check = !powerup5Check;
                    if (powerup5Check) { }
                    else { deleteProjector(); }
                }


                //check empty
                else if (slot2 == 0)
                {
                    print("Nothing in slot 2");
                }
            }
        }
    }


    //Drop object
    void Powerup1()
    {

        if (playerN.playerNumber == 1)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = viewCam.ScreenPointToRay(Input.mousePosition); //Main camera to mouse position

                RaycastHit hitInfo = new RaycastHit();

                bool hit = Physics.Raycast(ray, out hitInfo); //Send data to hitInfo
                if (hit)
                {
                    Debug.DrawLine(ray.origin, hitInfo.point, Color.blue);

                    spawnPoint = hitInfo.point;
                    spawnPoint.y += 10;
                    Instantiate(powerup1, spawnPoint, Quaternion.identity);
                }
                powerup1Check = false;
                deleteProjector();

                if (slot1Triggered)
                {
                    slot1Triggered = false;
                    slot1 = 0;
                    DeleteSprite(1);
                    print("Used slot 1");
                }
                else if (slot2Triggered)
                {
                    slot2Triggered = false;
                    slot2 = 0;
                    DeleteSprite(2);
                    print("Used slot 2");
                }

            }
        }
        else if (playerN.playerNumber == 2)
        {

            if (Input.GetKey("joystick button 1"))
            {
                //Projection child = GetComponent<Projection>();
                //GameObject child = null;
                // child = this.transform.Find("AimingProjector").gameObject;

                // Vector3 temp = new Vector3(child.transform.position.x, child.transform.position.y, child.transform.position.z);

                
                {
                    //Vector3 spawnPoint = projector.transform.position;
                    spawnPoint = pj.transform.position;

                    spawnPoint.y = 10;

                    //print(spawnPoint.x + " " + spawnPoint.y + " " + spawnPoint.z);
                    Instantiate(powerup1, spawnPoint, Quaternion.identity);
                }
                powerup1Check = false;
                deleteProjector();

                if (slot1Triggered)
                {
                    slot1Triggered = false;
                    slot1 = 0;
                    DeleteSprite(1);
                    print("Used slot 1");
                }
                else if (slot2Triggered)
                {
                    slot2Triggered = false;
                    slot2 = 0;
                    DeleteSprite(2);
                    print("Used slot 2");
                }

            }
        }
    }

    void Powerup2()
    {
        if (playerN.playerNumber == 1)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = viewCam.ScreenPointToRay(Input.mousePosition); //Main camera to mouse position

                RaycastHit hitInfo = new RaycastHit();

                bool hit = Physics.Raycast(ray, out hitInfo); //Send data to hitInfo
                if (hit)
                {
                    Debug.DrawLine(ray.origin, hitInfo.point, Color.blue);

                    spawnPoint = hitInfo.point;
                    spawnPoint.y += 1;
                    Instantiate(powerup2, spawnPoint, Quaternion.identity);
                }
                powerup2Check = false;
                deleteProjector();

                if (slot1Triggered)
                {
                    slot1Triggered = false;
                    slot1 = 0;
                    DeleteSprite(1);
                    print("Used slot 1");
                }
                else if (slot2Triggered)
                {
                    slot2Triggered = false;
                    slot2 = 0;
                    DeleteSprite(2);
                    print("Used slot 2");
                }
            }
        }
        else if (playerN.playerNumber == 2)
        {
            if (Input.GetKey("joystick button 1"))
            {
               
                {
                    // Vector3 spawnPoint = projector.transform.position;
                    spawnPoint = pj.transform.position;

                    spawnPoint.y = 5;
                    Instantiate(powerup2, spawnPoint, Quaternion.identity);
                }
                powerup2Check = false;
                deleteProjector();

                if (slot1Triggered)
                {
                    slot1Triggered = false;
                    slot1 = 0;
                    DeleteSprite(1);
                    print("Used slot 1");
                }
                else if (slot2Triggered)
                {
                    slot2Triggered = false;
                    slot2 = 0;
                    DeleteSprite(2);
                    print("Used slot 2");
                }

            }
        }
    }

    void Powerup3()
    {
        if (playerN.playerNumber == 1)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = viewCam.ScreenPointToRay(Input.mousePosition); //Main camera to mouse position

                RaycastHit hitInfo = new RaycastHit();

                bool hit = Physics.Raycast(ray, out hitInfo); //Send data to hitInfo
                if (hit)
                {
                    Debug.DrawLine(ray.origin, hitInfo.point, Color.blue);

                    spawnPoint = hitInfo.point;
                    spawnPoint.y += 1;
                    Instantiate(powerup3, spawnPoint, Quaternion.identity);
                }
                powerup3Check = false;
                deleteProjector();

                if (slot1Triggered)
                {
                    slot1Triggered = false;
                    slot1 = 0;
                    DeleteSprite(1);
                    print("Used slot 1");
                }
                else if (slot2Triggered)
                {
                    slot2Triggered = false;
                    slot2 = 0;
                    DeleteSprite(2);
                    print("Used slot 2");
                }

            }
        }
        else if (playerN.playerNumber == 2)
        {
            if (Input.GetKey("joystick button 1"))
            {
                
                {
                    spawnPoint = pj.transform.position;
                    spawnPoint.y = 1;
                    Instantiate(powerup3, spawnPoint, Quaternion.identity);
                }
                powerup3Check = false;
                deleteProjector();

                if (slot1Triggered)
                {
                    slot1Triggered = false;
                    slot1 = 0;
                    DeleteSprite(1);
                    print("Used slot 1");
                }
                else if (slot2Triggered)
                {
                    slot2Triggered = false;
                    slot2 = 0;
                    DeleteSprite(2);
                    print("Used slot 2");
                }

            }  
        }
    }
    void Powerup4()
    {
        if (playerN.playerNumber == 1)
        {
            if (Input.GetMouseButtonDown(0))
            {
                this.GetComponent<PlayerController>().shield = true;
                //  player.GetComponent<PlayerController>().shield = true;
                print("Shield On");

                powerup4Check = false;
                projector.SetActive(false);

                if (slot1Triggered)
                {
                    slot1Triggered = false;
                    slot1 = 0;
                    DeleteSprite(1);
                    print("Used slot 1");
                }
                else if (slot2Triggered)
                {
                    slot2Triggered = false;
                    slot2 = 0;
                    DeleteSprite(2);
                    print("Used slot 2");
                }

            }
        }
        else if(playerN.playerNumber == 2)
        {
            if (Input.GetKey("joystick button 1"))
            {
                this.GetComponent<PlayerController>().shield = true;
                //  player.GetComponent<PlayerController>().shield = true;
                print("Shield On");

                powerup4Check = false;
                projector.SetActive(false);

                if (slot1Triggered)
                {
                    slot1Triggered = false;
                    slot1 = 0;
                    DeleteSprite(1);
                    print("Used slot 1");
                }
                else if (slot2Triggered)
                {
                    slot2Triggered = false;
                    slot2 = 0;
                    DeleteSprite(2);
                    print("Used slot 2");
                }

            }
        }
    }
    void Powerup5()
    {
        if (playerN.playerNumber == 1)
        {
            if (Input.GetMouseButtonDown(0))
            {
                player.GetComponent<PlayerController>().speedBoost = true;
                print("Boost On");

                powerup5Check = false;
                projector.SetActive(false);

                if (slot1Triggered)
                {
                    slot1Triggered = false;
                    slot1 = 0;
                    DeleteSprite(1);
                    print("Used slot 1");
                }
                else if (slot2Triggered)
                {
                    slot2Triggered = false;
                    slot2 = 0;
                    DeleteSprite(2);
                    print("Used slot 2");
                }

            }
        }
        else if (playerN.playerNumber == 2)
        {
            if (Input.GetKey("joystick button 1"))
            {
                player.GetComponent<PlayerController>().speedBoost = true;
                print("Boost On");

                powerup5Check = false;
                projector.SetActive(false);

                if (slot1Triggered)
                {
                    slot1Triggered = false;
                    slot1 = 0;
                    DeleteSprite(1);
                    print("Used slot 1");
                }
                else if (slot2Triggered)
                {
                    slot2Triggered = false;
                    slot2 = 0;
                    DeleteSprite(2);
                    print("Used slot 2");
                }

            }
        }
    }






    //When collider touches the trigger
    void OnTriggerEnter(Collider collider)
    {
        //If collide, check if powerup slots are empty, if it is, fill it.

        //powerup 1
        if (collider.gameObject.name == "Powerup1")
        {
            if(slot1 == 0)
            {
                print("Picked up powerup 1 into slot1");
                slot1 = powerup1_Id;
                //ic.TriggerCreation(1, 0);
                CreateSprite(1, 0);
            }
            else if(slot2 == 0)
            {
                print("Picked up powerup 1 into slot2");
                slot2 = powerup1_Id;
                //ic.TriggerCreation(2, 0);
                CreateSprite(2, 0);
            }
            else
            {
                print("No space, cannot pick up");
            }
        }
        else if (collider.gameObject.name == "Powerup2")
        {
            if (slot1 == 0)
            {
                print("Picked up powerup 2 into slot1");
                slot1 = powerup2_Id;
                //ic.TriggerCreation(1, 0);
                CreateSprite(1, 1);
            }
            else if (slot2 == 0)
            {
                print("Picked up powerup 2 into slot2");
                slot2 = powerup2_Id;
                //ic.TriggerCreation(2, 0);
                CreateSprite(2, 1);
            }
            else
            {
                print("No space, cannot pick up");
            }
        }
        else if (collider.gameObject.name == "Powerup3")
        {
            if (slot1 == 0)
            {
                print("Picked up powerup 3 into slot1");
                slot1 = powerup3_Id;
                //ic.TriggerCreation(1, 0);
                CreateSprite(1, 2);
            }
            else if (slot2 == 0)
            {
                print("Picked up powerup 3 into slot2");
                slot2 = powerup3_Id;
                //ic.TriggerCreation(2, 0);
                CreateSprite(2, 2);
            }
            else
            {
                print("No space, cannot pick up");
            }
        }
        else if (collider.gameObject.name == "Powerup4")
        {
            if (slot1 == 0)
            {
                print("Picked up powerup 4 into slot1");
                slot1 = powerup4_Id;
                //ic.TriggerCreation(1, 0);
                CreateSprite(1, 3);
            }
            else if (slot2 == 0)
            {
                print("Picked up powerup 4 into slot2");
                slot2 = powerup4_Id;
                //ic.TriggerCreation(2, 0);
                CreateSprite(2, 3);
            }
            else
            {
                print("No space, cannot pick up");
            }
        }
        else if (collider.gameObject.name == "Powerup5")
        {
            if (slot1 == 0)
            {
                print("Picked up powerup 5 into slot1");
                slot1 = powerup5_Id;
                //ic.TriggerCreation(1, 0);
                CreateSprite(1, 4);
            }
            else if (slot2 == 0)
            {
                print("Picked up powerup 5 into slot2");
                slot2 = powerup5_Id;
                //ic.TriggerCreation(2, 0);
                CreateSprite(2, 4);
            }
            else
            {
                print("No space, cannot pick up");
            }
        }


    }

    public void CreateSprite(int slotNumber, int spriteID)
    {

        //GameObject sprite = Instantiate(spritePrefab) as GameObject;
        GameObject parent;
        sprite = Instantiate(spritePrefab) as GameObject;
        if (slotNumber == 1)
        {
            parent = ic.slot1;
            sprite.transform.parent = parent.transform;
        }
        else if (slotNumber == 2)
        {
            parent = ic.slot2;
            sprite.transform.parent = parent.transform;
        }

        //Sprite components
        sprite.name = "PowerupSprite";
        sprite.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        sprite.GetComponent<Image>().sprite = sprites[spriteID];
    }

    public void DeleteSprite(int slotNumber)
    {
        GameObject parent;
        if(slotNumber == 1)
        {
            parent = ic.slot1;
            if (parent.transform.FindChild("PowerupSprite").gameObject != null)
                Destroy(parent.transform.FindChild("PowerupSprite").gameObject);
        }
        if (slotNumber == 2)
        {
            parent = ic.slot2;
            if (parent.transform.FindChild("PowerupSprite").gameObject != null)
                Destroy(parent.transform.FindChild("PowerupSprite").gameObject);
        }
    }
}
