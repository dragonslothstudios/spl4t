﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;              
    private LevelGenerator levelManager;
    public PlayerController player1;
    public PlayerController player2;
    public Material mat1;
    public Material mat2;


    private Vector3 spawnP1;
    private Vector3 spawnP2;

    //Cinematic Camera
    GameObject mainCam;
    GameObject cinematicCam;
    Camera gameOverCam;
    PathFollower cinematicScript;


    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
       // DontDestroyOnLoad(gameObject);

        //Call the InitGame function to initialize the first level 
        InitGame();
    }

    void InitGame()
    {
        gameOverCam = GameObject.Find("GameOverCam").GetComponent<Camera>();
        gameOverCam.enabled = false;
        mainCam = GameObject.Find("Main Camera");
        cinematicCam = GameObject.Find("Cinematic Camera");
        cinematicCam.SetActive(true);
        cinematicScript = (PathFollower)cinematicCam.GetComponent(typeof(PathFollower));
        cinematicScript.cycle();

        InitPlayer1();
        InitPlayer2();
    }

    void InitPlayer1()
    {
        spawnP1 = new Vector3(-8, 1.1f, -8);
        player1.setControls("a", "s", "d");
        player1.spawnLocation = new Vector3(-8, 1, -8);
        player1.spawnLocation = spawnP1;
        player1.renderMat.material = mat1;
        player1.playerNumber = 1;
        player1.name = "Player 1";
        Instantiate(player1, spawnP1, Quaternion.identity);
    }

    void InitPlayer2()
    {
        spawnP2 = new Vector3(8, 1, 8);
        player2.setControls("joystick button 4", "joystick button 5", "joystick button 1");
        player2.spawnLocation = spawnP2;
        player1.renderMat.material = mat2;
        player2.playerNumber = 2;
        player2.name = "Player 2";
        Instantiate(player2, spawnP2, Quaternion.identity);
    }

    void GameOver()
    {
        player1.gameOver = true;
        player2.gameOver = true;
        //mainCam.SetActive(false);
        gameOverCam.enabled = true;

    }


    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
       // Debug.Log(cinematicScript.getTimer());

        if (cinematicScript.getTimer() >= 1)
        {
            cinematicCam.SetActive(false);
            mainCam.SetActive(true);
        }

        player1 = GameObject.Find("Player 1(Clone)").GetComponent<PlayerController>();
        player2 = GameObject.Find("Player 2(Clone)").GetComponent<PlayerController>();

        if (player1.lives == 0 && player2.lives == 0)
        {
            GameOver();
            print("Tied.");
        }
        else if (player1.lives == 0 && player2.lives > 0)
        {
            GameOver();
            print("P2 Wins");
        }
        else if (player2.lives == 0 && player1.lives > 0)
        {
            GameOver();
            print("P1 Wins");
        }
        else
        {

        }
    }
}
