﻿using UnityEngine;
using System.Collections;

public class Screenshot : MonoBehaviour {

    int count = 0;
    public int superSize = 12;

    void Update()
    {
        if (Input.GetKeyUp("c"))
        {
            Application.CaptureScreenshot("Screenshot" + count + ".png", superSize);
            count++;
        }
    }

    public void capture()
    {
        Application.CaptureScreenshot("Screenshot" + count + ".png", superSize);
    }

}
