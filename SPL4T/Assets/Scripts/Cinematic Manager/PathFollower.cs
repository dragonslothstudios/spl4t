﻿using UnityEngine;
using System.Collections;

public enum PathMode
{
    Once,
    Loop,
    PingPong
}

public class PathFollower : MonoBehaviour
{


    public BezierSpline spline;
    public PathMode mode;

    public bool lookForward; //have the gameobject face path direction

    public bool rotateAlong; //Have the gameobject right vector face path direction

    private bool goingForward = true;
    private bool stop = true;

    public float duration; 

    public float timer; //keep track of 0-1 interpolation value

    private void Update()
    {
        if (!stop)
        {
            if (goingForward)
            {
                timer += Time.deltaTime / duration;
                //loop from 0-1
                if (timer > 1f)
                {
                    if (mode == PathMode.Once) //stop if once
                    {
                        timer = 1f;
                        goingForward = false;
                        stop = true;
                    }
                    else if (mode == PathMode.Loop) //reset timer to loop
                    {
                        timer = 0f;
                    }
                    else //if ping pong
                    {
                        //timer = 2f - timer;
                        timer = 1f;
                        goingForward = false;
                    }
                }
            }
            else if(!goingForward && mode == PathMode.PingPong)//ping pong (going backwards)
            {
                timer -= Time.deltaTime / duration;
                if (timer < 0f)
                {
                    timer = 0;
                    goingForward = true;
                }
            }
            else if(!goingForward && mode == PathMode.Once) //To cycle between menus
            {
                timer -= Time.deltaTime / duration;
                if (timer < 0f)
                {
                    timer = 0;
                    goingForward = true;
                    stop = true;
                }
            }
        }

        Vector3 position = spline.GetPoint(timer);
        transform.localPosition = position;

        if (lookForward)
        {
            transform.LookAt(position + spline.GetDirection(timer));
        }

        if(rotateAlong)
        {
            transform.right = spline.GetDirection(timer);
        }
    }

    public void cycle()
    {
        stop = false;
        Debug.Log("Cycled");
    }

    public void SetSpline(BezierSpline s)
    {
        spline = s;
    }

    public float getTimer()
    {
        return timer;
    }
}


