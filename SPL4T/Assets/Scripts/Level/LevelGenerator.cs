﻿using UnityEngine;
using System.Collections;

public class LevelGenerator : MonoBehaviour {

    public Transform floorTile;
    public Vector3 levelSize;

    //public RespawnTiles respawner;

    [Range(0,1)]
    public float tileSize;

    void Start()
    {
        GenerateLevel();
        //respawner.SetChildren();
    }

    public void GenerateLevel()
    {
        //Creates an object to use as a folder
        string folderName = "Generated Level";
        if (transform.FindChild(folderName))
        {
            DestroyImmediate(transform.FindChild(folderName).gameObject);
        }

        Transform levelHolder = new GameObject (folderName).transform;
        levelHolder.parent = transform; 

        //Creates tile based level
        for(int x = 0; x < levelSize.x; x++)
        {
            for (int y = 0; y < levelSize.y; y++)
            {
                for (int z = 0; z < levelSize.z; z++)
                {
                    Vector3 tilePosition = new Vector3(-levelSize.x / 2 + 0.5f + x, -levelSize.y / 2 + 0.5f + y, -levelSize.z / 2 + 0.5f + z);
                    Transform newTile = Instantiate(floorTile, tilePosition, Quaternion.Euler(Vector3.zero)) as Transform;
                    newTile.name = floorTile.name;                    
                    //newTile.localScale = Vector3.one * (1 - tileSize); //edits size of tile
                    newTile.parent = levelHolder; //Put each tile under object folder
                }
            }
        }
    }
   
}
