﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuButtons : MonoBehaviour {

    AudioSource source0;
    bool loading = false;
    bool loadingDone = false;

    void Start()
    {
        source0 = GameObject.Find("Music").GetComponent<AudioSource>();
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (loading && source0.volume >= 0.11)
        {
            source0.volume = Mathf.Lerp(source0.volume, 0.1f, Time.deltaTime);
        }

        if(source0.volume < 0.11f)
        {
            loadingDone = true;
            LevelOne();
            print("loading done");
        }
    }

    public void LevelOne()
    {
        loading = true;

        if (loadingDone)
        {
            print("loading scene");
            SceneManager.LoadScene("LevelGenerator");
        }
    }

}
