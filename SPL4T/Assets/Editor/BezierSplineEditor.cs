﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BezierSpline))]
public class BezierSplineEditor : Editor
{
    private int steps = 10; //Steps between points

    private BezierSpline spline;
    private Transform transformHandle;
    private Quaternion rotationHandle;

    public override void OnInspectorGUI()
    {
        spline = target as BezierSpline;


        if (selectedIndex >= 0 && selectedIndex < spline.numberOfControlPoints())
        {
            EnableControlPointInspector();
        }
        if (GUILayout.Button("Add Curve"))
        {
            Undo.RecordObject(spline, "Add Curve");
            spline.AddCurve();
            //EditorUtility.SetDirty(spline);
        }

    }

    private void EnableControlPointInspector()
    {
        //allow changes to control points manually via typing in values
        EditorGUI.BeginChangeCheck();
        Vector3 selectedPoint = EditorGUILayout.Vector3Field("Position", spline.GetControlPoint(selectedIndex));
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(spline, "Move Control Point");
           // EditorUtility.SetDirty(spline);
            spline.SetControlPoint(selectedIndex, selectedPoint);
        }

        //control the mode
        EditorGUI.BeginChangeCheck();
        Modes mode = (Modes)EditorGUILayout.EnumPopup("Mode", spline.GetControlPointMode(selectedIndex));
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(spline, "Change Point Mode");
            spline.SetControlPointMode(selectedIndex, mode);
           // EditorUtility.SetDirty(spline);
        }
    }

    private void OnSceneGUI()
    {
        spline = target as BezierSpline;

        transformHandle = spline.transform;
        rotationHandle = Tools.pivotRotation == PivotRotation.Local ?
            transformHandle.rotation : Quaternion.identity;

        //Update points to handles
        Vector3 p0 = ShowPoint(0);
        //Loop over all the different curve segments
        for (int i = 1; i < spline.numberOfControlPoints(); i += 3)
        {
            Vector3 p1 = ShowPoint(i);
            Vector3 p2 = ShowPoint(i+1);
            Vector3 p3 = ShowPoint(i+2);

            //Draw line between points
            Handles.color = Color.grey;
            Handles.DrawLine(p0, p1);
            Handles.DrawLine(p2, p3);

            //Draw Curve
            Handles.DrawBezier(p0, p3, p1, p2, Color.white, null, 2);

            //Set start point to be the end point
            p0 = p3; 
        }


        ShowTangents();
    }

    private void ShowTangents()
    {
        Handles.color = Color.blue;
        Vector3 point = spline.GetPoint(0);
        Handles.DrawLine(point, point + spline.GetDirection(0));

        //to get equal number of tangents shown for each curve segment
        int s = steps * spline.CurveCount();
        for (int i = 1; i <= s; i++)
        {
            point = spline.GetPoint(i / (float)s);
            Handles.DrawLine(point, point + spline.GetDirection(i / (float)s));
        }
    }


    private Color[] colors =
    {
        Color.black,
        Color.yellow,
        Color.magenta
    };


    float buttonSize = 0.05f; //size of control point
    float selectionSize = 0.07f; //clickable size of control point
    int selectedIndex = -1; //the index of control point selected
    private Vector3 ShowPoint(int i)
    {
        //convert to world space    
        Vector3 point = transformHandle.TransformPoint(spline.GetControlPoint(i));

        //Get constant screen size on button
        float size = HandleUtility.GetHandleSize(point);
        Handles.color = colors[(int)spline.GetControlPointMode(i)];
        if (Handles.Button(point, rotationHandle, size*buttonSize, size*selectionSize, Handles.DotCap))
        {
            selectedIndex = i;
        }

        if (selectedIndex == i)
        {
            //check if handles were moved
            EditorGUI.BeginChangeCheck();
            point = Handles.DoPositionHandle(point, rotationHandle);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(spline, "Move Point");
               // EditorUtility.SetDirty(spline);
                //set new point to position of handle
                spline.SetControlPoint(i, transformHandle.InverseTransformPoint(point));
            }
        }
        return point;
    }

}