﻿using UnityEngine;
using System.Collections;

public class Trigger_Animation : MonoBehaviour
{
    private Animator anim;

    // Use this for initialization
    void Start()
    {
        anim.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter(Collider collider)
    {
        anim.enabled = true;
    }
}