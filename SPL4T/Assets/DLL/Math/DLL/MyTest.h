#pragma once
#ifdef FUNCDLL_EXPORT
#define DLL_API __declspec(dllexport) 
#else
#define DLL_API __declspec(dllimport) 
#endif

extern "C"
{
	DLL_API float TestMultiply(float a, float b);
	DLL_API float ReturnSpeed();

}


