﻿using UnityEngine;
using System.Collections;

public class Skydome : MonoBehaviour {
    public float turnSpeed = 0.001f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(Vector3.up, -turnSpeed * Time.deltaTime);
	}
}
